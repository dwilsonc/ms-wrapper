DROP TABLE IF EXISTS public.item_memory_entity;

CREATE TABLE public.item_memory_entity (
	id serial NOT NULL,
	item_id int4 NULL,
	"json" varchar(3000) NULL,
	CONSTRAINT item_memory_entity_pkey PRIMARY KEY (id)
);