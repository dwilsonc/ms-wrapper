package com.ms.wrapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@ComponentScan(basePackages = "com.ms.*")
@ConfigurationPropertiesScan("com.ms.*")
@EnableJpaRepositories("com.ms.*")
@EntityScan(basePackages = "com.ms.*")
@OpenAPIDefinition(info = @Info(title = "MS-WRAPPER", version = "2.0", description = "Microservicio wrapper"))
public class WrapperMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WrapperMsApplication.class, args);
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
