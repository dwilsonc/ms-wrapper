package com.ms.wrapper.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ms.wrapper.exception.BadRequestException;
import com.ms.wrapper.exception.NotFoundException;
import com.ms.wrapper.resource.ItemMemoryResponseResource;
import com.ms.wrapper.resource.mapper.ItemMemoryResourceMapper;
import com.ms.wrapper.service.ItemMemoryService;
import com.ms.wrapper.util.NumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class ItemController
 *
 * @author denniswilson
 *
 */
@RestController
public class ItemMemoryController {

    @Autowired
    private ItemMemoryService itemMemoryService;

    /**
     * Method that get item by id from Redis
     *
     * @param id
     * @return
     * @throws NotFoundException
     */
    @GetMapping(value = "/api/v1/wrapper/vip/item/{id}")
    public ResponseEntity<ItemMemoryResponseResource> getItem(@PathVariable("id") String id) throws BadRequestException, NotFoundException, JsonProcessingException {

        if(!NumberUtil.validateOnlyNumbers(id))
            throw new BadRequestException("Formato incorrecto, ingrese solo numeros");

        return ResponseEntity.ok(ItemMemoryResourceMapper.toResource(itemMemoryService.findByItemId(Integer.parseInt(id))));
    }
}
