package com.ms.wrapper.consumer;

import com.ms.wrapper.config.PropertiesConfiguration;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Class KafkaConsumerConfig
 *
 * @author denniswilson
 *
 */
@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Autowired
    private PropertiesConfiguration properties;

    @Bean
    public ConsumerFactory<String, KafkaObject> consumerFactory() {
        JsonDeserializer<KafkaObject> deserializer = new JsonDeserializer<>(KafkaObject.class);
        deserializer.setRemoveTypeHeaders(false);
        deserializer.addTrustedPackages("*");
        deserializer.setUseTypeMapperForKey(true);

        Map<String, Object> configMap = new HashMap<>();
        configMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers());
        configMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        configMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deserializer);
        configMap.put(ConsumerConfig.GROUP_ID_CONFIG, properties.getWrapperGroupId());

        return new DefaultKafkaConsumerFactory<>(configMap, new StringDeserializer(), deserializer);

    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, KafkaObject> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, KafkaObject> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}