package com.ms.wrapper.consumer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

/**
 * Class KafkaObject
 *
 * @author denniswilson
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class KafkaObject {

    @JsonCreator
    public KafkaObject(@JsonProperty("id") int id,
                       @JsonProperty("message") String message) {
        super();
        this.id = id;
        this.message = message;
    }

    /**
     * ID
     */
    @JsonProperty("id")
    private int id;

    /**
     * MESSAGE
     */
    @JsonProperty("message")
    private String message;

    @JsonProperty("id")
    public int getId() {
        return id;
    }
    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }
    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }
}
