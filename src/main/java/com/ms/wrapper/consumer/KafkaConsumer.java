package com.ms.wrapper.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.wrapper.client.ItemsClient;
import com.ms.wrapper.constantes.Constantes;
import com.ms.wrapper.dto.ItemResponseDto;
import com.ms.wrapper.entity.ItemMemoryEntity;
import com.ms.wrapper.service.ItemMemoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * Class KafkaConsumer
 *
 * @author denniswilson
 *
 */
@Component
public class KafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    private ItemMemoryService itemMemoryService;

    @Autowired
    private ItemsClient itemClient;

    @KafkaListener(topics = "${spring.kafka.topic.name}")
    public void listenTopic(KafkaObject kafkaObject) throws JsonProcessingException {
        LOGGER.info("Recieved Message of topic in  listener: {}", kafkaObject.getId() + "/" + kafkaObject.getMessage());
        ItemResponseDto itemResponseDto = itemClient.getItemById(kafkaObject.getId());
        ObjectMapper obj = new ObjectMapper();
        String jsonResponse = obj.writeValueAsString(itemResponseDto);

        if(Constantes.KAFKA_MESSAGE_NEW.equals(kafkaObject.getMessage()))
            itemMemoryService.save(ItemMemoryEntity.builder().itemId(kafkaObject.getId()).json(jsonResponse).build());
        else if(Constantes.KAFKA_MESSAGE_UPDATE.equals(kafkaObject.getMessage()))
            itemMemoryService.update(kafkaObject.getId(), jsonResponse);
    }
}
