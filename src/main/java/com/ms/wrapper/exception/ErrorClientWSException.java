package com.ms.wrapper.exception;

/**
 * Class ErrorClientWSException
 *
 * @author denniswilson
 *
 */
public class ErrorClientWSException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final String msg;

    public ErrorClientWSException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

}