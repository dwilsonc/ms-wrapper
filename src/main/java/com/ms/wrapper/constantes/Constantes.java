package com.ms.wrapper.constantes;

/**
 * Class Constantes
 *
 * @author denniswilson
 *
 */
public class Constantes {

    /**
     * Constant KAFKA MESSAGE NEW
     */
    public static final String KAFKA_MESSAGE_NEW = "new";

    /**
     * Constant KAFKA MESSAGE UPDATE
     */
    public static final String KAFKA_MESSAGE_UPDATE = "update";
}
