package com.ms.wrapper.entity;

import lombok.*;

import javax.persistence.*;

/**
 * Class ItemMemoryEntity
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class ItemMemoryEntity {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    /**
     * ITEM_ID
     */
    @Column(name = "ITEM_ID")
    private Integer itemId;

    /**
     * JSON
     */
    @Column(name = "JSON", length = 3000)
    private String json;
}
