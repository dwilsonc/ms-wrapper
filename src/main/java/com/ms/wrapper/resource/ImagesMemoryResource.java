package com.ms.wrapper.resource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class ImagesResource
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImagesMemoryResource {

    /**
     * ID
     */
    private Integer id;

    /**
     * URL
     */
    private String url;
}
