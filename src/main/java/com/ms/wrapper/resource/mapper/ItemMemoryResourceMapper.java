package com.ms.wrapper.resource.mapper;

import com.ms.wrapper.dto.ItemResponseDto;
import com.ms.wrapper.resource.ItemMemoryResponseResource;
import org.modelmapper.ModelMapper;

/**
 * Class ItemResourceMapper
 *
 * @author denniswilson
 *
 */
public class ItemMemoryResourceMapper {

    /**
     * Mapper that transform ItemResponseDto to ItemMemoryResponseResource
     * @param itemResponseDto
     * @return ItemResponseResource
     */
    public static ItemMemoryResponseResource toResource(ItemResponseDto itemResponseDto) {
        ModelMapper mapper = new ModelMapper();
        return mapper.map(itemResponseDto, ItemMemoryResponseResource.class);
    }
}
