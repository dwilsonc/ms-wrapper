package com.ms.wrapper.resource;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Class ItemResponseDto
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({"id", "sku","name", "price", "currency", "symbol","thumbnail", "description", "images" })
public class ItemMemoryResponseResource {

    /**
     * ID
     */
    private Integer id;

    /**
     * SKU
     */
    private String sku;

    /**
     * NAME
     */
    private String name;

    /**
     * PRICE
     */
    private Double price;

    /**
     * CURRENCY
     */
    @JsonProperty("currency")
    private String currencyCadena;

    /**
     * SYMBOL
     */
    private String symbol;

    /**
     * THUMBNAIL
     */
    private String thumbnail;

    /**
     * DESCRIPTION
     */
    private String description;

    /**
     * IMAGES
     */
    private List<ImagesMemoryResource> images;
}
