package com.ms.wrapper.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class PropertiesConfiguration
 *
 * @author denniswilson
 *
 */
@ConfigurationProperties
@Getter
@Setter
public class PropertiesConfiguration {

    @Value("${item.client.host}")
    private String host;

    @Value("${item.client.login.url}")
    private String loginEndpoint;

    @Value("${item.client.getItem.url}")
    private String getItemEndpoint;

    @Value("${item.client.jwt.user}")
    private String jwtUser;

    @Value("${item.client.jwt.password}")
    private String jwtPassword;

    @Value("${spring.redis.host}")
    private String redisHost;

    @Value("${spring.redis.port}")
    private int redisPort;

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.consumer.group-id}")
    private String wrapperGroupId;
}
