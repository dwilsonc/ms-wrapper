package com.ms.wrapper.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.wrapper.config.PropertiesConfiguration;
import com.ms.wrapper.dto.ItemResponseDto;
import com.ms.wrapper.dto.LoginRequestDto;
import com.ms.wrapper.dto.LoginResponseDto;
import com.ms.wrapper.exception.ErrorClientWSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

/**
 * Class ItemsClient
 *
 * @author denniswilson
 *
 */
@Component
public class ItemsClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemsClient.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PropertiesConfiguration properties;

    /**
     * Client Login JWT
     *
     * @return
     * @throws JsonProcessingException
     */
    private String getTokenJWT() {

        ResponseEntity<LoginResponseDto> response;

        try {
            LoginRequestDto authenticationUser = getAuthenticationUser();
            String authenticationBody = getBody(authenticationUser);
            HttpHeaders authenticationHeaders = getHeaders();
            HttpEntity<String> authenticationEntity = new HttpEntity<>(authenticationBody,
                    authenticationHeaders);

            response = restTemplate.exchange(properties.getHost().concat(properties.getLoginEndpoint()),
                    HttpMethod.POST, authenticationEntity, LoginResponseDto.class);

        } catch (RestClientResponseException e) {
            LOGGER.error("[Error] de tipo Http al momento de obtener el token JWT {} ",e.getMessage());
            LOGGER.debug("  ResponseBody: {}",e.getResponseBodyAsString());
            LOGGER.trace("  Exception:",e);
            throw new ErrorClientWSException("[Error] de tipo Http de obtener el token JWT");
        } catch (Exception e) {
            LOGGER.error("[Error] al momento de obtener un item por id: {}" ,e.getMessage());
            throw new ErrorClientWSException("[Error] al momento de obtener el token JWT");
        }

        return Optional.ofNullable(response.getBody()).map(LoginResponseDto::getJwttoken).orElse("");
    }

    /**
     * Client Get Item By Id
     *
     * @param itemId
     * @return
     * @throws JsonProcessingException
     */
    public ItemResponseDto getItemById(Integer itemId) {

        ResponseEntity<ItemResponseDto> response;

        try {
            HttpHeaders headers = getHeaders();
            headers.set("Authorization", getTokenJWT());

            HttpEntity<String> jwtEntity = new HttpEntity<>(headers);

            response = restTemplate.exchange(
                    properties.getHost().concat(properties.getGetItemEndpoint().concat(String.valueOf(itemId))),
                    HttpMethod.GET, jwtEntity, ItemResponseDto.class);

        } catch (RestClientResponseException e) {
            LOGGER.error("[Error] de tipo Http al momento de obtener un item por id: {} ",e.getMessage());
            LOGGER.debug("  ResponseBody: {}",e.getResponseBodyAsString());
            LOGGER.trace("  Exception:",e);
            throw new ErrorClientWSException("[Error] de tipo Http al momento de obtener un item por id");

        } catch (Exception e) {
            LOGGER.error("[Error] al momento de obtener un item por id: {}" ,e.getMessage());
            throw new ErrorClientWSException("[Error] al momento de obtener un item por id");
        }

        return response.getBody();
    }

    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }

    private LoginRequestDto getAuthenticationUser() {
        return LoginRequestDto.builder().username(properties.getJwtUser()).password(properties.getJwtPassword()).build();
    }

    private String getBody(final LoginRequestDto user) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(user);
    }
}
