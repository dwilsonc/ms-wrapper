package com.ms.wrapper.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class CurrencyDto
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyDto {

    /**
     * IDE
     */
    private Integer id;

    /**
     * CODE
     */
    private String code;

    /**
     * SYMBOL
     */
    private String symbol;
}
