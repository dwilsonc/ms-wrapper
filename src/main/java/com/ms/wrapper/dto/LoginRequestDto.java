package com.ms.wrapper.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Class LoginRequestDto
 *
 * @author denniswilson
 *
 */
@Data
@Builder
public class LoginRequestDto implements Serializable {

    private static final long serialVersionUID = 5926468583005150707L;

    /**
     * USERNAME
     */
    private String username;

    /**
     * PASSWORD
     */
    private String password;

}
