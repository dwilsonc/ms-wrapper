package com.ms.wrapper.dto;

import lombok.*;

import java.util.List;

/**
 * Class ItemResponseDto
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItemResponseDto {

    /**
     * ID
     */
    private Integer id;

    /**
     * SKU
     */
    private String sku;

    /**
     * NAME
     */
    private String name;

    /**
     * PRICE
     */
    private Double price;

    /**
     * CURRENCY
     */
    private CurrencyDto currency;

    /**
     * CURRENCY
     */
    private String currencyCadena;

    /**
     * SYMBOL
     */
    private String symbol;

    /**
     * THUMBNAIL
     */
    private String thumbnail;

    /**
     * DESCRIPTION
     */
    private String description;

    /**
     * IMAGES
     */
    private List<ImagesDto> images;
}
