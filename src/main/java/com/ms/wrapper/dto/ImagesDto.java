package com.ms.wrapper.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Class ImagesDto
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImagesDto {

    /**
     * ID
     */
    private Integer id;

    /**
     * URL
     */
    private String url;
}
