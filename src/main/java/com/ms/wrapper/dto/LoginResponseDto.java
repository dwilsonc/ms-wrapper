package com.ms.wrapper.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Class LoginResponseDto
 *
 * @author denniswilson
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginResponseDto implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;

    /**
     * JWTTOKEN
     */
    private String jwttoken;

}

