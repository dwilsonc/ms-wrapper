package com.ms.wrapper.repository;

import com.ms.wrapper.entity.ItemMemoryEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Currency ItemMemoryRepository
 *
 * @author denniswilson
 */
@Repository
public interface ItemMemoryRepository extends CrudRepository<ItemMemoryEntity, String> {

    /**
     * Method that find ItemMemory by itemId
     *
     * @param itemId
     * @return
     */
    Optional<ItemMemoryEntity> findByItemId(Integer itemId);

    /**
     * Method that update ItemMemory by itemId
     * @param itemId
     */
    @Transactional
    @Modifying
    @Query("update ItemMemoryEntity i set i.json= ?2 where i.itemId = ?1")
    void updateItemByItemId(Integer itemId, String json);
}
