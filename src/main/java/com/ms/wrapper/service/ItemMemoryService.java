package com.ms.wrapper.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ms.wrapper.dto.ItemResponseDto;
import com.ms.wrapper.entity.ItemMemoryEntity;
import com.ms.wrapper.exception.NotFoundException;

/**
 * Class ItemMemoryService
 *
 * @author denniswilson
 *
 */
public interface ItemMemoryService {

    public void save (ItemMemoryEntity itemMemoryEntity);

    public void update (Integer itemId, String json);

    ItemResponseDto findByItemId(Integer itemId) throws NotFoundException, JsonProcessingException ;
}
