package com.ms.wrapper.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ms.wrapper.dto.ItemResponseDto;
import com.ms.wrapper.entity.ItemMemoryEntity;
import com.ms.wrapper.exception.NotFoundException;
import com.ms.wrapper.repository.ItemMemoryRepository;
import com.ms.wrapper.service.ItemMemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.cache.annotation.CacheConfig;

/**
 * Class ItemMemoryServiceImpl
 *
 * @author denniswilson
 *
 */
@Service
@CacheConfig(cacheNames = "itemCache")
public class ItemMemoryServiceImpl implements ItemMemoryService {

    @Autowired
    private ItemMemoryRepository itemRepository;

    public void save (ItemMemoryEntity itemMemoryEntity){
        itemRepository.save(itemMemoryEntity);
    }

    /**
     * Methot that update item memory
     * @param itemId
     * @param json
     */
    @Override
    @CacheEvict(cacheNames = "itemMemoryEntity", allEntries = true)
    public void update(Integer itemId, String json) {
        itemRepository.updateItemByItemId(itemId,json);
    }

    /**
     * Method that get item by id
     *
     * @param itemId
     * @return
     * @throws NotFoundException
     * @throws JsonProcessingException
     */
    @Override
    @Cacheable(cacheNames = "itemMemoryEntity", key = "#itemId", unless = "#result == null")
    public ItemResponseDto findByItemId(Integer itemId) throws NotFoundException, JsonProcessingException {
        ItemMemoryEntity itemMemoryEntity = itemRepository.findByItemId(itemId).orElseThrow(() -> new NotFoundException("No se encontro el item - " + itemId));
        ObjectMapper objectMapper = new ObjectMapper();
        ItemResponseDto itemResponseDto = objectMapper.readValue(itemMemoryEntity.getJson(), ItemResponseDto.class);
        itemResponseDto.setSymbol(itemResponseDto.getCurrency().getSymbol());
        itemResponseDto.setCurrencyCadena(itemResponseDto.getCurrency().getCode());
        return itemResponseDto;
    }
}
