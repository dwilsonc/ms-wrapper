package com.ms.wrapper.consumer


import com.ms.wrapper.client.ItemsClient
import com.ms.wrapper.dto.ItemResponseDto
import com.ms.wrapper.service.ItemMemoryService
import spock.lang.Specification


class KafkaConsumerTest extends Specification {

    private KafkaConsumer kafkaConsumer = new KafkaConsumer()
    ItemMemoryService itemMemoryService = Mock()
    ItemsClient itemClient = Mock()

    void setup() {
        this.kafkaConsumer.itemMemoryService = this.itemMemoryService
        this.kafkaConsumer.itemClient = this.itemClient
    }

    def "Listen kafka topic new"() {
        given:
        itemClient.getItemById(1) >> ItemResponseDto.builder().build()

        expect: "ejecuto la funcionalidad"
        kafkaConsumer.listenTopic(new KafkaObject(1,"new"))
    }

    def "Listen kafka topic update"() {
        given:
        itemClient.getItemById(1) >> ItemResponseDto.builder().build()

        expect: "ejecuto la funcionalidad"
        kafkaConsumer.listenTopic(new KafkaObject(1,"update"))
    }
}