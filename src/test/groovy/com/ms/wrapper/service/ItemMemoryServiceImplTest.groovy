package com.ms.wrapper.service

import com.ms.wrapper.dto.ItemResponseDto
import com.ms.wrapper.entity.ItemMemoryEntity
import com.ms.wrapper.exception.NotFoundException
import com.ms.wrapper.repository.ItemMemoryRepository
import com.ms.wrapper.service.impl.ItemMemoryServiceImpl
import spock.lang.Specification


class ItemMemoryServiceImplTest extends Specification {

    private ItemMemoryServiceImpl itemServiceImpl = new ItemMemoryServiceImpl()
    ItemMemoryRepository itemRepository = Mock()
    private String jsonResponse = "{\"id\":1,\"sku\":\"998877665544\",\"name\":\"Pelota amarillo de playa\",\"price\":8000.0,\"currency\":{\"id\":2,\"code\":\"EUR\",\"symbol\":\"€\"},\"currencyCadena\":null,\"symbol\":null,\"thumbnail\":\"http://cloud.images.google.com/thumbnail_pelotaplayaarenaamarillo.png\",\"description\":\"Pelota amarillo de playa redonda\",\"images\":[{\"id\":373,\"url\":\"http://cloud.images.google.com/pelotaplayaamarillo41.png\"},{\"id\":374,\"url\":\"http://cloud.images.google.com/pelotaplayaamarillo51.png\"},{\"id\":375,\"url\":\"http://cloud.images.google.com/pelotaplayaamarillo61.png\"}]}";

    void setup() {
        this.itemServiceImpl.itemRepository = this.itemRepository
    }

    def "[Save Item] Save Item by ID"() {
        expect: "ejecuto la funcionalidad"
        itemServiceImpl.save(ItemMemoryEntity.builder().build())
    }

    def "[Update Item] Update Item by ID in Memory"() {
        expect: "ejecuto la funcionalidad"
        itemServiceImpl.update(1,jsonResponse)
    }

    def "[Get Item] Get Item by ID in Memory"() {
        given:
        Optional<ItemMemoryEntity> result = Optional.of(ItemMemoryEntity.builder().id(1).json(jsonResponse).build());
        itemRepository.findByItemId(1) >> result

        when: "ejecuto la funcionalidad"
        ItemResponseDto resp = itemServiceImpl.findByItemId(1)

        then: "resultado esperado"
        resp
    }

    def "[Get Item] Id no encontrado"() {
        given:
        itemRepository.findByItemId(1) >> new Optional<ItemMemoryEntity>()

        when: "ejecuto la funcionalidad"
        itemServiceImpl.findByItemId(1)

        then: "resultado esperado"
        thrown(NotFoundException)
    }
}