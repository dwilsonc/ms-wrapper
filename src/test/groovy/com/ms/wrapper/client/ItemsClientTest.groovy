package com.ms.wrapper.client

import com.ms.wrapper.config.PropertiesConfiguration
import com.ms.wrapper.dto.ItemResponseDto
import com.ms.wrapper.dto.LoginResponseDto
import com.ms.wrapper.exception.ErrorClientWSException
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestClientResponseException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification


class ItemsClientTest extends Specification {

    private ItemsClient itemsClient = new ItemsClient()
    RestTemplate restTemplate = Mock()
    PropertiesConfiguration properties = Mock()

    void setup() {
        this.itemsClient.restTemplate = this.restTemplate
        this.itemsClient.properties = this.properties

        properties.getJwtUser() >> "dwilsonc"
        properties.getJwtPassword() >> "123456Ab"
        properties.getHost() >> "http://localhost:8084"
        properties.getLoginEndpoint() >> "/nisum/api/v1/authenticate"

        ResponseEntity<LoginResponseDto> responseEntityLogin = Mock() {
            getBody() >> LoginResponseDto.builder().jwttoken("Bearer xxxxxxxxxxxx").build()
            getStatusCode() >> HttpStatus.OK
        }

        restTemplate.exchange(_,HttpMethod.POST,_,_) >> responseEntityLogin
    }

    def "Get token JWT from MS-ITEMS"() {
        expect: "ejecuto la funcionalidad"
        itemsClient.getTokenJWT()
    }

   def "Get Item by Id from MS-ITEMS"() {
        given:
        itemsClient.getTokenJWT() >> "Bearer xxxxxxxxxxxx"

        and: "Se mockea responseEntitytItem"
        properties.getGetItemEndpoint() >> "/nisum/api/v1/item/"

        ResponseEntity<ItemResponseDto> responseEntityItem = Mock() {
            getBody() >> ItemResponseDto.builder().build()
            getStatusCode() >> HttpStatus.OK
        }

        restTemplate.exchange(_, HttpMethod.GET,_,_) >> responseEntityItem

        expect: "ejecuto la funcionalidad"
        itemsClient.getItemById(1)
    }

    def "[Exception] Get Item by Id from MS-ITEMS"() {
        given:
        itemsClient.getTokenJWT() >> "Bearer xxxxxxxxxxxx"

        and: "Se mockea responseEntitytItem"
        properties.getGetItemEndpoint() >> null

        when: "ejecuto la funcionalidad"
        itemsClient.getItemById(1)

        then: "resultado esperado"
        thrown(ErrorClientWSException)
    }
}
