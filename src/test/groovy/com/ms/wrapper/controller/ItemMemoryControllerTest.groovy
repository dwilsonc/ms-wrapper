package com.ms.wrapper.controller

import com.ms.wrapper.dto.ItemResponseDto
import com.ms.wrapper.exception.BadRequestException
import com.ms.wrapper.resource.ItemMemoryResponseResource
import com.ms.wrapper.service.ItemMemoryService
import org.springframework.http.ResponseEntity
import spock.lang.Specification


class ItemMemoryControllerTest extends Specification {

    private ItemMemoryController itemMemoryController = new ItemMemoryController()
    ItemMemoryService itemMemoryService = Mock()

    void setup() {
        this.itemMemoryController.itemMemoryService = this.itemMemoryService
    }

    def "[Get Item] Get Item By ID in Memory"() {
        given:
        itemMemoryService.findByItemId(1) >> ItemResponseDto.builder().build()

        when: "ejecuto la funcionalidad"
        ResponseEntity<ItemMemoryResponseResource> resp = itemMemoryController.getItem("1")

        then: "resultado esperado"
        resp.statusCode.value() == 200
    }

    def "[Get Item] Invalid Format in PathVariable"() {
        when: "ejecuto la funcionalidad"
        itemMemoryController.getItem("1a")

        then: "resultado esperado"
        thrown(BadRequestException)
    }
}