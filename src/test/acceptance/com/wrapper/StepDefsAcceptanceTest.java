package com.wrapper;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import com.wrapper.util.MsItemsHttpClient;


public class StepDefsAcceptanceTest {

    protected static String token;

    @Autowired
    private MsItemsHttpClient httpClient;

    @Given("autenticar con usuario {string} y password {string}")
    public void autenticarUsuarioConRutYPassword(String usuario, String password) {
        token = httpClient.getToken(usuario, password);
    }

    @Then("validar token")
    public void validarToken() {
        Assert.assertNotNull(token);
    }

}
