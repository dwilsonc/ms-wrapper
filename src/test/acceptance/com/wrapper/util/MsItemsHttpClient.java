package com.wrapper.util;

import com.ms.wrapper.dto.LoginRequestDto;
import com.ms.wrapper.dto.LoginResponseDto;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
public class MsItemsHttpClient {

    private final String SERVER_URL = "http://localhost:8084";
    private final String LOGIN_ENDPOINT = "/nisum/api/v1/authenticate";

    @LocalServerPort
    private int port;
    private final RestTemplate restTemplate = new RestTemplate();

    private String thingsEndpoint() {
        return SERVER_URL + ":" + port + LOGIN_ENDPOINT;
    }

    public String getToken(String usuario, String password) {
        return restTemplate.postForEntity(thingsEndpoint(),
                LoginRequestDto.builder().username(usuario).password(password).build(),
                LoginResponseDto.class).getBody().getJwttoken();
    }
}
