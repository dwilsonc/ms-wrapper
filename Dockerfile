FROM gradle:jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -x test

FROM adoptopenjdk/openjdk11:latest

EXPOSE 8085

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/*.jar /app/ms-wrapper.jar

ENTRYPOINT ["java", "-jar","/app/ms-wrapper.jar"]