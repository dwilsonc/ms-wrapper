## MS-WRAPPER

Esto es una prueba de concepto que demuestra como implementar un microservicio consumer usando Spring Boot, PostgreSQL y adicionalmente Redis.

Contiene la lógica general de un consumidor kafka donde consume el topico `items-update`, a partir del id obtenido, consume el servicio obtener Item del `ms-items` y lo registra en la base datos PostgreSQL en formato json. Luego con en el endpoint `/api/v1/wrapper/vip/item/{id}` obtiene el objeto item guardado en BD, consultando la primera vez y las siguientes veces de la cache. Adicionalmente cada vez que el objeto se actualice por medio del listener kafka, automáticamente la cache se actualiza.

Method	| Path	| Description	|
------------- | ------------------------- | ------------- |
GET	| /api/v1/wrapper/vip/item/{id}	| Get item by id	|

#### Notas

- Es un API REST que se comunica con otro microservicio de manera asincrona a traves de una cola kafka.
- Se usa Redis como base datos en memoria.

## ¿Como correr este microservicio?

Hay que iniciar con docker estas 2 aplicaciones Spring Boot (MS-WRAPPER) y Redis.

#### Antes de que inicies

- Instala Docker y Docker Compose.
- Offset Explorer 2, permite visualizar los objetos y navegar con los objetos en tu Apache Kafka Cluster.

#### Ponlo a correr

1. Clonar el proyecto: `git clone https://dwilsonc@bitbucket.org/dwilsonc/ms-wrapper.git`

2. Posicionarse sobre la ruta donde se encuentra el dockerfile y ejecutar el siguiente comando docker para construir la imagen del microservicio MS-WRAPPER

   `docker build -t ms-wrapper .`
   
3. Iniciemos Redis y el MS-WRAPPER, escribiendo el comando `docker-compose up -d` desde donde se encuentra ubicado el fichero `docker-compose.yml`.

    
    Opcional

    Podemos usar el comando nc para verificar que ambos servidores (kafka y Zookeper) estén escuchando en los puertos respectivos:

   `nc -z localhost 22181`

    Connection to localhost port 22181 [tcp/*] succeeded!

   `nc -z localhost 29092`

    Connection to localhost port 29092 [tcp/*] succeeded!

#### Ejecucion en local

Ir a la carpeta `/src/postman` e importar la coleccion en postman.

- [http://localhost:8085/nisum](http://localhost:8085/nisum) - Aplicación Spring Boot

### Swagger

Puedes consultar los endpoints accediendo a swagger: 

- [http://localhost:8085/nisum/swagger-ui.html](http://localhost:8085/nisum/swagger-ui.html)

### Cobertura

Generado desde la carpeta `/build/jacocoHtml/index.html`

![jacoco.png](jacoco.png)

